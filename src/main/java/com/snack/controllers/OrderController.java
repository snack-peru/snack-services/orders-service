package com.snack.controllers;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import com.snack.dto.ControllerSuborderDTO;
import com.snack.dto.ControllerSuborderLineDTO;
import com.snack.dto.OrderDTO;
import com.snack.dto.RegisterTotalOrderDto;
import com.snack.dto.SuborderDTO;
import com.snack.dto.SuborderLineDTO;
import com.snack.entities.Order;
import com.snack.entities.Suborder;
import com.snack.entities.SuborderLine;
import com.snack.services.OrderService;
import com.snack.services.SuborderLineService;
import com.snack.services.SuborderService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.HttpClientErrorException;

import io.swagger.annotations.ApiOperation;
import org.springframework.http.MediaType;

@RestController
public class OrderController {
    
    @Autowired
    private OrderService orderService;

    @Autowired
    private SuborderService suborderService;

    @Autowired
    private SuborderLineService suborderLineService;



    //Author: Daniel
    @GetMapping(value = "/api/order/{idCustomer}/{idOrderStatus}")
    @ApiOperation(produces = MediaType.APPLICATION_JSON_VALUE, value = "Obtiene todas las ordenes de un cliente segun su estado de orden(En proceso / Historial de Pedidos)",
            notes = "Se debe enviar como parametro la id del cliente y la id del estado de las ordenes",
            responseContainer = "Object",
            response = Void.class)
    public List<Order> getAllOrdersByIdCustomerIdOrderStatus(@PathVariable("idCustomer") Long idCustomer, 
    @PathVariable ("idOrderStatus") Integer idOrderStatus){
        return orderService.getAllOrdersByIdCustomerIdOrderStatus(idCustomer,idOrderStatus);
    }

    //Author: Daniel
    @PostMapping(value = "/api/order/{idCustomer}")
    @ApiOperation(produces = MediaType.APPLICATION_JSON_VALUE, value = "Guarda una orden de un cliente , conteniendo sus Subordenes y estas conteniendo Lineas de Suborden",
            notes = "Se debe enviar como parametro la id del cliente y un request a guardar que contendra las subordenes(por tiendas) y cada una lineas de suborden (productos a comprar)",
            responseContainer = "Object",
            response = Void.class)
    public ResponseEntity<?> saverOrder(@PathVariable("idCustomer") Long idCustomer,@RequestBody RegisterTotalOrderDto registerTotalOrderDto){
        try {

            Integer i= orderService.findForeignKeysOrder(registerTotalOrderDto);

            if(i==1){
                
                OrderDTO registerOrderDto = new OrderDTO();
    
                registerOrderDto.setIdOrderStatus(registerTotalOrderDto.getOrderidOrderStatus());
                registerOrderDto.setOrderDate(registerTotalOrderDto.getOrderDate());
                registerOrderDto.setOrderHour(registerTotalOrderDto.getOrderHour());
                registerOrderDto.setTotal(registerTotalOrderDto.getOrdertotal());

                Order order = orderService.saveOrder(idCustomer,registerOrderDto);
                registerTotalOrderDto.setIdOrder(order.getIdOrder());
                List<SuborderDTO> listRegisterSuborderDto = new ArrayList<SuborderDTO>();

                for (ControllerSuborderDTO csdto: registerTotalOrderDto.getControllerSuborderDto()){
                    csdto.setSuborderidCustomer(idCustomer);
                    SuborderDTO registerSuborderDto = new SuborderDTO();
                    registerSuborderDto.setDeliveryPrice(csdto.getSuborderdeliveryPrice());
                    registerSuborderDto.setDisplayId(csdto.getSuborderdisplayId());
                    registerSuborderDto.setIdCustomer(idCustomer);
                    registerSuborderDto.setIdDeliveryType(csdto.getSuborderidDeliveryType());
                    registerSuborderDto.setIdPaymentType(csdto.getSuborderidPaymentType());
                    registerSuborderDto.setIdStore(csdto.getSuborderidStore());
                    registerSuborderDto.setIdSuborderStatus(csdto.getSuborderidSuborderStatus());
                    registerSuborderDto.setQuantity(csdto.getSuborderquantity());
                    registerSuborderDto.setSubtotal(csdto.getSubordersubtotal());
                    registerSuborderDto.setTotal(csdto.getSubordertotal());
                    listRegisterSuborderDto.add(registerSuborderDto);
                }
                List<Suborder> listSuborder = suborderService.saveSuborder(order.getIdOrder(),listRegisterSuborderDto);
                
                for (Integer indice=0;indice<listSuborder.size();indice++){
                    registerTotalOrderDto.getControllerSuborderDto().get(indice).setIdSuborder(listSuborder.get(indice).getIdSuborder());
                }
                
                for (ControllerSuborderDTO csdto: registerTotalOrderDto.getControllerSuborderDto()){
                    List<SuborderLineDTO> listRegisterSuborderLineDto = new ArrayList<SuborderLineDTO>();

                    for(ControllerSuborderLineDTO csldto: csdto.getControllerSuborderLineDto()){
                        csldto.setSuborderlineidStore(csdto.getSuborderidStore());
                        SuborderLineDTO registerSuborderLineDto = new SuborderLineDTO();
                        registerSuborderLineDto.setQuantityBought(csldto.getSuborderlinequantityBought());
                        registerSuborderLineDto.setUnitPrice(csldto.getSuborderlineunitPrice());
                        registerSuborderLineDto.setQuantityAvailable(csldto.getSuborderlinequantityAvailable());
                        registerSuborderLineDto.setIdStore(csdto.getSuborderidStore());
                        registerSuborderLineDto.setIdProduct(csldto.getSuborderlineidProduct());
                        registerSuborderLineDto.setIdSuborderLineStatus(csldto.getSuborderlineidSuborderLineStatus());
                        listRegisterSuborderLineDto.add(registerSuborderLineDto);
                    }
                    List<SuborderLine> listSuborderline = suborderLineService.saveSuborderLine(csdto.getIdSuborder(), listRegisterSuborderLineDto);
                    
                    for (Integer indice=0;indice<listSuborderline.size();indice++){
                        csdto.getControllerSuborderLineDto().get(indice).setIdSuborderLine(listSuborderline.get(indice).getIdSuborderLine());
                    }

                }

                return new ResponseEntity<RegisterTotalOrderDto>(registerTotalOrderDto, HttpStatus.CREATED);
            }
            else{
                
                return new ResponseEntity <>("Claves foraneas invalidas",HttpStatus.NOT_ACCEPTABLE);
            }

            
        } catch (HttpClientErrorException e) {
            return ResponseEntity.status(e.getStatusCode()).body(Collections.singletonMap("message", e.getResponseBodyAsString()));
        }
    }



}