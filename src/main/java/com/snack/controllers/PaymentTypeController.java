package com.snack.controllers;

import com.snack.dto.ListOfIds;
import com.snack.dto.RegisterTotalOrderDto;
import com.snack.entities.PaymentType;
import com.snack.services.PaymentTypeService;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Arrays;
import java.util.List;

@RestController
public class PaymentTypeController {
    @Autowired
    private PaymentTypeService paymentTypeService;

    // Author: Martin
    @GetMapping(value="/api/paymentType/{idPaymentType}")
    @ApiOperation(produces = MediaType.APPLICATION_JSON_VALUE, value = "Obtiene un metodo de pago por su Id",
            notes = "Se envia como path param el ID del metodo de pago",
            responseContainer = "Object",
            response = PaymentType.class)
    public PaymentType getPaymentType(@PathVariable(name="idPaymentType") Integer idPaymentType) {
        return paymentTypeService.getById(idPaymentType);
    }

    // Author: Martin
    @GetMapping(value="/api/paymentType/type/{type}")
    @ApiOperation(produces = MediaType.APPLICATION_JSON_VALUE, value = "Obtiene una lista de los metodos de pago segun su tipo 'cash' o 'card'",
            notes = "Se envia como path param las cadenas 'cash' o 'card' dependiendo cual se desee obtener",
            responseContainer = "Object",
            response = Void.class)
    public List<PaymentType> getPaymentTypeByType(@PathVariable(name="type") String type) {
        return paymentTypeService.getByType(type);
    }

    @PostMapping(value = "/api/paymentType/get-by-ids")
    @ApiOperation(produces = MediaType.APPLICATION_JSON_VALUE, value = "Obtiene el detalle de uno o mas metodos de pago segun sus IDs",
            notes = "Se debe enviar como parametro la lista de IDs de los metodos de pago de los que se desee su detalle",
            responseContainer = "Object",
            response = Void.class)
    public List<PaymentType> getPaymentTypesById (@RequestBody ListOfIds list){
        return paymentTypeService.getByListOfId(list.getIds());
    }
}
