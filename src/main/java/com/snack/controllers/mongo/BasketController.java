package com.snack.controllers.mongo;

import com.snack.dto.mongo.request.AddProductRequest;
import com.snack.dto.mongo.request.AssignProductsToStoreRequest;
import com.snack.dto.mongo.request.ProductstoExclude;
import com.snack.entities.mongo.Basket;
import com.snack.services.mongo.BasketService;
import org.springframework.beans.factory.annotation.Autowired;

import io.swagger.annotations.ApiOperation;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

@RestController
@CrossOrigin
@RequestMapping(value = "/api/basket")
public class BasketController {

    @Autowired
    private BasketService basketService;

    // Author: Sergio
    @PostMapping(value = "/{customerId}")
    @ApiOperation(produces = MediaType.APPLICATION_JSON_VALUE, value = "Guarda o actualiza la cesta de un cliente con los productos",
            notes = "Se debe enviar como parametro el id del cliente y un request a guardar que contendra los nuevos productos a guardar en la cesta",
            responseContainer = "Object",
            response = Void.class)
    public void newProduct(@PathVariable(name = "customerId") Long customerId, @RequestBody AddProductRequest addProductRequest) {
        // validar si el cliente ya tiene una cesta, si ya la tiene, traerla, sino instanciar el objeto
        Basket basket = basketService.getBasketByCustomerId(customerId);
        if (basket == null) {
            // se crea una nueva cesta
            basketService.addNewProductToNewBasket(customerId, addProductRequest);
        } else {
            // se añade el producto a la cesta
            basketService.addProductToBasket(addProductRequest, basket);
        }
    }
    
    //Author: Daniel
    @GetMapping(value = "/{customerId}")
    @ApiOperation(produces = MediaType.APPLICATION_JSON_VALUE, value = "Obtiene la cesta del cliente",
            notes = "Se debe enviar como parametro el id del cliente",
            responseContainer = "Object",
            response = Void.class)
    public Basket getBasket(@PathVariable(name = "customerId") Long customerId) {
        Basket basket = basketService.getBasketByCustomerId(customerId);
        return basket;
    }

    //Author: Fabricio
    @PutMapping (value =  "/{customerId}")
    @ApiOperation(produces = MediaType.APPLICATION_JSON_VALUE, value = "Elimina productos de la cesta del cliente",
            notes = "Se debe enviar como parametro la id del cliente y un request donde se enviaran una lista de productos a eliminar",
            responseContainer = "Object",
            response = Void.class)
    public void deleteProductBasket(@PathVariable(name = "customerId") Long customerId, @RequestBody ProductstoExclude productsDelete) {
        System.out.println(productsDelete);
        basketService.deleteProductsofBasket(customerId, productsDelete);
    }

    // Author: Sergio
    @PostMapping(value = "/{customerId}/assignProductsToStore")
    @ApiOperation(produces = MediaType.APPLICATION_JSON_VALUE, value = "Asignar una lista de productos de la canasta a una tienda",
            notes = "Se debe enciar el customerId en la uri, el deliveryType como RequestParam y la lista de productos " +
                    "y el id de la tienda en el requestBody",
            responseContainer = "Object",
            response = void.class)
    public void assignProductsToStore(@PathVariable(name = "customerId") Long customerId,
                                      @RequestParam("deliveryType") String deliveryType,
                                      @RequestBody AssignProductsToStoreRequest request) {
        basketService.assignProductsToStore(customerId, deliveryType, request);
    }
}
