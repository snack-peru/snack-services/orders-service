package com.snack.controllers;

import java.util.List;

import com.snack.dto.SuborderLineDTO;
import com.snack.entities.SuborderLine;
import com.snack.services.SuborderLineService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import io.swagger.annotations.ApiOperation;
import org.springframework.http.MediaType;


@RestController
public class SuborderLineController {
    
    @Autowired
    private SuborderLineService suborderLineService;
    
    //Author: Daniel
    @GetMapping(value = "/api/suborderline/{idSuborder}")
    @ApiOperation(produces = MediaType.APPLICATION_JSON_VALUE, value = "Obtiene todas las lineas de suborden de una suborden",
            notes = "Se debe enviar como parametro la id de la suborden",
            responseContainer = "Object",
            response = Void.class)
    public List<SuborderLine> getAllSuborderLinesByIdSuborder(@PathVariable("idSuborder") Long idSuborder){
        return suborderLineService.getAllSuborderLinesByIdSuborder(idSuborder);
    }

    //Author: Daniel
    @PatchMapping(value = "/api/suborderline/{idSuborderLine}")
    @ApiOperation(produces = MediaType.APPLICATION_JSON_VALUE, value = "Actualiza el estado de una linea de suborden",
            notes = "Se debe enviar como parametro la id de la linea de suborden y el id del estado de la linea de suborden al que cambiara",
            responseContainer = "Object",
            response = Void.class)
    public void updateSuborderLine(@PathVariable("idSuborderLine") Long idSuborderLine, @RequestParam(value = "idSuborderLineStatus") Integer idSuborderLineStatus){
        suborderLineService.updateSuborderLine(idSuborderLine,idSuborderLineStatus);
    }

    //Author: Daniel
    @PostMapping(value = "/api/suborderline/{idSuborder}")
    @ApiOperation(produces = MediaType.APPLICATION_JSON_VALUE, value = "Guarda las lineas de suborden de una suborden",
            notes = "Se debe enviar como parametro la id de la suborden y la lista de lineas de suborden a guardar",
            responseContainer = "Object",
            response = Void.class)
    public List<SuborderLine> saveSuborderLine(@PathVariable("idSuborder") Long idSuborder,@RequestBody List<SuborderLineDTO> suborderLineDTO){
        return suborderLineService.saveSuborderLine(idSuborder,suborderLineDTO);
    }

}