package com.snack.controllers;

import java.util.List;

import com.snack.dto.SuborderDTO;
import com.snack.entities.Suborder;
import com.snack.services.SuborderService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.annotations.ApiOperation;
import org.springframework.http.MediaType;


@RestController
public class SuborderController {

    @Autowired
    private SuborderService suborderService;
    
    //Author: Daniel
    @GetMapping(value = "/api/suborder/{idOrder}")
    @ApiOperation(produces = MediaType.APPLICATION_JSON_VALUE, value = "Obtiene todas las subordenes de una orden",
            notes = "Se debe enviar como parametro la id de la orden",
            responseContainer = "Object",
            response = Void.class)
    public List<Suborder> getAllSubordersByIdOrder(@PathVariable("idOrder") Long idOrder){
        return suborderService.getAllSubordersByIdOrder(idOrder);
    }

    //Author: Daniel
    @PatchMapping(value = "/api/suborder/{idSuborder}")
    @ApiOperation(produces = MediaType.APPLICATION_JSON_VALUE, value = "Actualiza el estado de una suborden",
            notes = "Se debe enviar como parametro la id de la suborden y el id del estado de suborden al que cambiara",
            responseContainer = "Object",
            response = Void.class)
    public void updateSuborderStatus(@PathVariable("idSuborder") Long idSuborder, @RequestParam(value = "idSuborderStatus") Integer idSuborderStatus){
        suborderService.updateSuborderStatus(idSuborder,idSuborderStatus);
    }

    //Author: Daniel
    @PostMapping(value = "/api/suborder/{idOrder}")
    @ApiOperation(produces = MediaType.APPLICATION_JSON_VALUE, value = "Guarda varias subordenes",
            notes = "Se debe enviar como parametro la id de la orden y la lista de subordenes a guardar",
            responseContainer = "Object",
            response = Void.class)
    public List<Suborder> saveSuborder (@PathVariable ("idOrder") Long idOrder, @RequestBody List<SuborderDTO> suborderDTO){
        return suborderService.saveSuborder(idOrder,suborderDTO);
    }
    
}