package com.snack.repositories.mongo;

import com.snack.entities.mongo.Store;

import org.springframework.data.mongodb.repository.MongoRepository;

public interface StoreRepository extends MongoRepository<Store, Integer> {
    
}