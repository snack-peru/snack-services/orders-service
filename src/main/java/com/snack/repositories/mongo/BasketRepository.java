package com.snack.repositories.mongo;

import com.snack.entities.mongo.Basket;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface BasketRepository extends MongoRepository<Basket, String> {

    Basket findByCustomerId(Long customerId);
}
