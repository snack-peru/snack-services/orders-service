package com.snack.repositories.mongo;

import com.snack.entities.mongo.Product;

import org.springframework.data.mongodb.repository.MongoRepository;

public interface ProductRepository extends MongoRepository<Product, Long> {
    
}