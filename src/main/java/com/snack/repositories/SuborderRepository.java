package com.snack.repositories;

import java.util.List;

import com.snack.entities.Suborder;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository
public interface SuborderRepository extends JpaRepository <Suborder, Long>  {
    
    @Query(value= "select * from suborder where id_order= ?1", nativeQuery = true)
    public List<Suborder> getAllSubordersByIdOrder(Long idOrder);

    
    @Transactional
    @Modifying
    @Query(value= "update  suborder set id_suborder_status=?2 where id_suborder= ?1 ", nativeQuery = true)
    public void updateSuborderStatus(Long idSuborder, Integer idSuborderStatus);
    //Recordar que falta la tabla penalidades , cuando exista se debe anidar este metodo para que devuelva la penalidad a pagar y no un void (DANIEL)
}
