package com.snack.repositories;

import com.snack.entities.SuborderLineStatus;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
@Repository
public interface SuborderLineStatusRepository extends JpaRepository <SuborderLineStatus,Integer>{

}
    
