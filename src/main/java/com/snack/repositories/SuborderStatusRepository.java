package com.snack.repositories;

import com.snack.entities.SuborderStatus;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface SuborderStatusRepository extends JpaRepository <SuborderStatus,Integer>{
    
}