package com.snack.repositories;

import java.util.List;

import com.snack.entities.SuborderLine;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository
public interface SuborderLineRepository extends JpaRepository <SuborderLine, Long> {

    @Query(value= "select * from suborder_line where id_suborder= ?1", nativeQuery = true)
    public List<SuborderLine> getAllSuborderLinesByIdSuborder(Long idSuborder);

    @Transactional
    @Modifying
    @Query(value= "update  suborder_line set id_suborder_line_status=?2 where id_suborder_line= ?1", nativeQuery = true)
    public void updateSuborderLine(Long idSuborderLine, Integer idSuborderLineStatus);
}