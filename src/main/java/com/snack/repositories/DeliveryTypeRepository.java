package com.snack.repositories;

import com.snack.entities.DeliveryType;

import org.springframework.data.jpa.repository.JpaRepository;

public interface DeliveryTypeRepository extends JpaRepository<DeliveryType, Integer> {
    
}