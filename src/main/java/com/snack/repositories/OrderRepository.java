package com.snack.repositories;

import java.util.List;

import com.snack.entities.Order;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface OrderRepository extends JpaRepository <Order,Long> {
    
    @Query(value= "select * from orders where id_customer= ?1 and id_order_status= ?2", nativeQuery = true)
    public List<Order> getAllOrdersByIdCustomerIdOrderStatus(Long idCustomer, Integer idOrderStatus);

}