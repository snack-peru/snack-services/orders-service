package com.snack.repositories;

import com.snack.entities.PaymentType;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface PaymentTypeRepository extends JpaRepository <PaymentType,Integer>{
    public Optional<PaymentType> findById(Integer id);
    public List<PaymentType> findByType(String type);

    @Query(value= "select * from payment_type where id_payment_type IN ?1", nativeQuery = true)
    public List<PaymentType> findByListOfIds(List<Long> list);
}
    
