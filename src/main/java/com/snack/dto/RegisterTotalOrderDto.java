package com.snack.dto;

import java.sql.Date;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonFormat;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class RegisterTotalOrderDto {
    @JsonFormat (shape=JsonFormat.Shape.STRING, pattern="dd/MM/yyyy")
    private Date orderDate;
    private String orderHour;
    private Double ordertotal;
    private Long idOrder;
    private Integer orderidOrderStatus;
    private List<ControllerSuborderDTO> controllerSuborderDto;
    
}