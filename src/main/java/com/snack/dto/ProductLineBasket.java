package com.snack.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class ProductLineBasket {
    private Integer quantity;
    private String units;
    private String branch;
    private Long productId;
    private String productName;
}