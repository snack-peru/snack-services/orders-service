package com.snack.dto;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class SuborderDTO {
    private String displayId;
    private Double subtotal;
    private Double deliveryPrice;
    private Double total;
    private Integer quantity;
    private Integer idStore;
    private Long idCustomer;
    private Integer idPaymentType;
    private Integer idDeliveryType;
    private Integer idSuborderStatus;

   
}
