package com.snack.dto.mongo.request;

import java.util.List;

import com.snack.dto.StoreProductsToExcludeDTO;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Builder
public class ProductstoExclude {
    private List<Long> listProducts;
    private List<StoreProductsToExcludeDTO> storeProducts;
}
