package com.snack.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class SuborderLineDTO {
    private Integer quantityBought;
    private Double unitPrice;
    private Integer quantityAvailable;
    private Integer idStore;
    private Long idProduct;
    private Long idSuborder;
    private Integer idSuborderLineStatus;

    

}