package com.snack.services;

import java.util.ArrayList;
import java.util.List;

import com.snack.dto.SuborderDTO;
import com.snack.entities.DeliveryType;
import com.snack.entities.Order;
import com.snack.entities.PaymentType;
import com.snack.entities.Suborder;
import com.snack.entities.SuborderStatus;
import com.snack.repositories.SuborderRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class SuborderService {
    
    @Autowired
    private SuborderRepository suborderRepository;


    public List<Suborder> getAllSubordersByIdOrder(Long idOrder){
        return suborderRepository.getAllSubordersByIdOrder(idOrder);
    }

    public void updateSuborderStatus(Long idSuborder, Integer idSuborderStatus){
         suborderRepository.updateSuborderStatus(idSuborder,idSuborderStatus);
    }

    public List<Suborder> saveSuborder(Long idOrder, List<SuborderDTO> suborderDTO){
        List<Suborder> SubordersToReturn = new ArrayList<Suborder>();

        for(SuborderDTO suborDTO: suborderDTO){
            Order order = new Order();
            PaymentType paymentType = new PaymentType ();
            SuborderStatus suborderStatus= new SuborderStatus ();
            DeliveryType deliveryType= new DeliveryType ();
            Suborder suborder = new Suborder();
            order.setIdOrder(idOrder);
            paymentType.setIdPaymentType(suborDTO.getIdPaymentType());
            suborderStatus.setIdSuborderStatus(suborDTO.getIdSuborderStatus());
            deliveryType.setIdDeliveryType(suborDTO.getIdDeliveryType());
            suborder.setDisplayId(suborDTO.getDisplayId());
            suborder.setSubtotal(suborDTO.getSubtotal());
            suborder.setDeliveryPrice(suborDTO.getDeliveryPrice());
            suborder.setTotal(suborDTO.getTotal());
            suborder.setQuantity(suborDTO.getQuantity());
            suborder.setIdStore(suborDTO.getIdStore());
            suborder.setIdCustomer(suborDTO.getIdCustomer());
            suborder.setPaymentType(paymentType);
            suborder.setDeliveryType(deliveryType);
            suborder.setSuborderStatus(suborderStatus);
            suborder.setOrder(order);
            SubordersToReturn.add(suborderRepository.save(suborder));
        }

        return SubordersToReturn;
    }
}