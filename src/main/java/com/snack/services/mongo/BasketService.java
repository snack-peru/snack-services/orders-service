package com.snack.services.mongo;

import com.snack.dto.StoreProductsToExcludeDTO;
import com.snack.dto.mongo.request.AddProductRequest;
import com.snack.dto.mongo.request.AssignProductsToStoreRequest;
import com.snack.dto.mongo.request.ProductstoExclude;
import com.snack.entities.mongo.Basket;
import com.snack.entities.mongo.Product;
import com.snack.entities.mongo.Store;
import com.snack.repositories.mongo.BasketRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class BasketService {

    @Autowired
    private BasketRepository basketRepository;

    public Basket getBasketByCustomerId(Long customerId) {
        return basketRepository.findByCustomerId(customerId);
    }

    public void addNewProductToNewBasket(Long customerId, AddProductRequest addProductRequest) {
        Basket basket;
        Product product = Product.builder().productId(addProductRequest.getProductId())
                .brand(addProductRequest.getBrand()).measurement(addProductRequest.getMeasurement())
                .name(addProductRequest.getName()).quantity(addProductRequest.getQuantity()).build();

        List<Product> productList = new ArrayList<Product>();
        productList.add(product);

        // validar si tiene tienda asginada
        if (addProductRequest.getStoreId() == null) {
            // cuando no tiene una tienda asignada

            basket = Basket.builder().customerId(customerId).productsAlone(productList).stores(null).build();
        } else {
            // cuando si tiene una tienda asignada
            Store store = Store.builder().storeId(addProductRequest.getStoreId()).name(addProductRequest.getStoreName())
                    .products(productList).build();

            List<Store> storeList = new ArrayList<Store>();
            storeList.add(store);

            basket = Basket.builder().customerId(customerId).stores(storeList).build();
        }

        // guardar basket en la base de datos
        basketRepository.save(basket);
    }

    public void addProductToBasket(AddProductRequest addProductRequest, Basket basket) {
        Product product = Product.builder().productId(addProductRequest.getProductId())
                .name(addProductRequest.getName()).quantity(addProductRequest.getQuantity())
                .measurement(addProductRequest.getMeasurement()).brand(addProductRequest.getBrand()).build();

        // validar si el producto viene con tienda asignada
        if (addProductRequest.getStoreId() == null) {
            // cuando el producto NO viene con una tienda asignada
            // traer los productos sin tienda de la canasta del cliente
            List<Product> productList = basket.getProductsAlone();
            List<Product> productListUpdated = new ArrayList<Product>();
            if (productList != null) {
                productListUpdated = productList.stream()
                        .filter(p -> p.getProductId() == addProductRequest.getProductId()).collect(Collectors.toList());
            }

            if (!(productList == null) && !productListUpdated.isEmpty()) {
                // el producto si existe en la canasta
                productList = productList.stream().map(p -> {
                    if (p.getProductId() == addProductRequest.getProductId()) {
                        p.setQuantity(p.getQuantity() + addProductRequest.getQuantity());
                    }
                    return p;
                }).collect(Collectors.toList());
            } else {
                // el producto no existe en la canasta
                if (productList == null) {
                    productList = new ArrayList<Product>();
                }
                productList.add(product);
            }

            basket.setProductsAlone(productList);
            basketRepository.save(basket);

        } else {
            // cuando el producto viene con una tienda asignada
            // buscar si la tienda ya existe en la canasta del cliente
            List<Store> storeList;
            if (basket.getStores() == null) {
                storeList = new ArrayList<Store>();
            } else {
                storeList = basket.getStores().stream()
                        .filter(store -> store.getStoreId() == addProductRequest.getStoreId()
                                && store.getDeliverytype().equalsIgnoreCase(addProductRequest.getDeliveryType()))
                        .collect(Collectors.toList());
            }

            List<Product> productList;

            if (storeList == null || storeList.isEmpty()) {
                // si la tienda no existe, se crea la tienda
                productList = new ArrayList<Product>();
                productList.add(product);

                Store newStore = Store.builder().storeId(addProductRequest.getStoreId())
                        .name(addProductRequest.getStoreName()).products(productList)
                        .deliverytype(addProductRequest.getDeliveryType()).build();

                storeList.add(newStore);
                List<Store> stores;
                if (basket.getStores() == null) {
                    stores = new ArrayList<Store>();
                } else {
                    stores = basket.getStores();
                }

                stores.add(newStore);
                basket.setStores(stores);

                productList = Collections.EMPTY_LIST;
            } else {
                productList = storeList.get(0).getProducts().stream()
                        .filter(p -> p.getProductId() == addProductRequest.getProductId()).collect(Collectors.toList());
            }

            List<Product> productListUpdated;

            if (!productList.isEmpty()) {
                // el producto si existe en la canasta
                productListUpdated = storeList.get(0).getProducts().stream().map(p -> {
                    if (p.getProductId() == addProductRequest.getProductId()) {
                        p.setQuantity(p.getQuantity() + addProductRequest.getQuantity());
                    }
                    return p;
                }).collect(Collectors.toList());
            } else {
                // el producto no existe en la canasta
                productListUpdated = storeList.get(0).getProducts();
                productListUpdated.add(product);
            }

            List<Store> storeListUpdated = basket.getStores().stream().map(s -> {
                if (s.getStoreId() == addProductRequest.getStoreId()) {
                    s.setProducts(productListUpdated);
                }
                return s;
            }).collect(Collectors.toList());

            basket.setStores(storeListUpdated);
            basketRepository.save(basket);
        }
    }

    public void deleteProductsofBasket(Long customerId, ProductstoExclude productsDelete) {
        Basket basket = basketRepository.findByCustomerId(customerId);
        System.out.println("Entro a Service " + productsDelete);
        //Eliminando productos sin tienda
        List<Product> productList = basket.getProductsAlone();
        List<Long> idProductsDel = productsDelete.getListProducts();
        List<Product> newListProducts = new ArrayList<Product>();

        for (Product P: productList){
            Long idProductBasket = P.getProductId();
            
            for (Long idProduct: idProductsDel)
            {   
                if(idProductBasket == idProduct){
                    newListProducts.add(P);
                }
            }
        }

        for (Product Prod: newListProducts){
            productList.remove(Prod);
        }

        basket.setProductsAlone(productList);

       //Eliminando productos con tienda
       List<StoreProductsToExcludeDTO> storeWithProductsList = productsDelete.getStoreProducts();
       List<Store> storeList = basket.getStores();
       List<Store> newStoreList = new ArrayList<Store>();

       HashMap<Integer,List<Long>> storeMap = new HashMap<Integer,List<Long>>();
       for(StoreProductsToExcludeDTO storeWithProductsFor : storeWithProductsList ){
           storeMap.put(storeWithProductsFor.getStoreId(),storeWithProductsFor.getListProducts());
       }


       for(Store storeListFor : storeList){

           try{
                List<Long> productsToDelete = storeMap.get(storeListFor.getStoreId());
                try{
                    List<Product> newProductListByStore = storeListFor.getProducts();
                    HashMap<Long,Product> storeProductMap = new HashMap<Long,Product>();
                    for(Product productFor : storeListFor.getProducts()){
                        storeProductMap.put(productFor.getProductId(),productFor);
                    }

                    for(Long productsToDeleteFor : productsToDelete){
                        try{
                            newProductListByStore.remove(storeProductMap.get(productsToDeleteFor));
                        }
                        catch(Exception e){
                            System.out.println("ProductId enviado desde front invalido en cesta");
                        }
                    }


                    storeListFor.setProducts(newProductListByStore);
                }
                catch(Exception e){
                    System.out.println("Fallo intermedio");
                }

           }
           catch(Exception e){
               System.out.println("No hay productos de la tienda "+storeListFor.getStoreId()+" por eliminar");
           }

           if(storeListFor.getProducts().size() >=1){
            newStoreList.add(storeListFor);
           }
           
       }
       basket.setStores(newStoreList);

       //Guardando Cambios
       basketRepository.save(basket);
    }

    public void assignProductsToStore(Long customerId, String deliveryType, AssignProductsToStoreRequest request) {
        Basket basket = basketRepository.findByCustomerId(customerId);

        // obtener los productsAlone correspondientes para asignarlos a la canasta
        List<Product> productsAlone = basket.getProductsAlone().stream()
                .filter(product -> request.getProductsId().contains(product.getProductId()))
                .collect(Collectors.toList());

        // obtener las tiendas
        List<Store> stores;
        if (basket.getStores() == null) {
            stores = null;
        } else {
            stores = basket.getStores().stream().filter(store -> store.getStoreId() == request.getStoreId() &&
                    store.getDeliverytype().equalsIgnoreCase(deliveryType))
                    .collect(Collectors.toList());
        }
        System.out.println(stores);
        System.out.println(stores.size());
        System.out.println(stores.isEmpty());

        // validar si ya existe la tienda
        if (stores == null || stores.isEmpty()) {
            // cuando no existe la tienda en la cesta
            Store newStore = Store.builder()
                    .storeId(request.getStoreId())
                    .name(request.getStoreName())
                    .deliverytype(deliveryType)
                    .products(productsAlone)
                    .build();

            if (stores == null) {
                List<Store> newStoreList = new ArrayList<>();
                basket.setStores(newStoreList);
            }

            basket.getStores().add(newStore);
        } else {
            // si existe la tienda en la cesta
            for (Store store:basket.getStores()) {
                if (store.getStoreId() == stores.get(0).getStoreId()) {
                    store.getProducts().addAll(productsAlone);
                }
            }
        }

        // eliminar los productos de productsAlone
        basket.getProductsAlone().removeIf(e -> request.getProductsId().contains(e.getProductId()));

        // Guardar canasta
        basketRepository.save(basket);
    }
}
