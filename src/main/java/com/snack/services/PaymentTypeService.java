package com.snack.services;

import com.snack.entities.PaymentType;
import com.snack.repositories.PaymentTypeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class PaymentTypeService {
    @Autowired
    private PaymentTypeRepository paymentTypeRepository;

    public PaymentType getById (Integer idPaymentType) {
        Optional<PaymentType> pay = paymentTypeRepository.findById(idPaymentType);
        return pay.isPresent()? pay.get() : null;
    }

    public List<PaymentType> getByType (String type) {
        return paymentTypeRepository.findByType(type);
    }

    public List<PaymentType> getByListOfId (List<Long> idList) {
        return paymentTypeRepository.findByListOfIds(idList);
    }
}
