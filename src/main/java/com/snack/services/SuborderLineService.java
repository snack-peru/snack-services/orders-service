package com.snack.services;

import java.util.ArrayList;
import java.util.List;

import com.snack.dto.SuborderLineDTO;
import com.snack.entities.Suborder;
import com.snack.entities.SuborderLine;
import com.snack.entities.SuborderLineStatus;
import com.snack.repositories.SuborderLineRepository;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class SuborderLineService {
    
    @Autowired
    private SuborderLineRepository suborderLineRepository;




    public List<SuborderLine> getAllSuborderLinesByIdSuborder(Long idSuborder){
        return suborderLineRepository.getAllSuborderLinesByIdSuborder(idSuborder);
    }

    public void updateSuborderLine(Long idSuborderLine, Integer idSuborderLineStatus){
         suborderLineRepository.updateSuborderLine(idSuborderLine,idSuborderLineStatus);
    }

    public List<SuborderLine> saveSuborderLine (Long idSuborder, List<SuborderLineDTO> suborderLineDTO){

        List<SuborderLine> suborderLineToReturn = new ArrayList<SuborderLine>();

        for(SuborderLineDTO suborlineDTO: suborderLineDTO){
            SuborderLine suborderLine = new SuborderLine();
            Suborder suborder = new Suborder();
            SuborderLineStatus suborderLineStatus = new SuborderLineStatus();
    
            suborder.setIdSuborder(idSuborder);
            suborderLineStatus.setIdSuborderLineStatus(suborlineDTO.getIdSuborderLineStatus());
            suborderLine.setSuborder(suborder);
            suborderLine.setSuborderLineStatus(suborderLineStatus);
            suborderLine.setQuantityBought(suborlineDTO.getQuantityBought());
            suborderLine.setUnitPrice(suborlineDTO.getUnitPrice());
            suborderLine.setQuantityAvailable(suborlineDTO.getQuantityAvailable());
            suborderLine.setIdStore(suborlineDTO.getIdStore());
            suborderLine.setIdProduct(suborlineDTO.getIdProduct());
            suborderLineToReturn.add(suborderLineRepository.save(suborderLine));
        }
        return suborderLineToReturn;
    }
}