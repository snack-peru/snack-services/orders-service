package com.snack.services;

import java.util.List;
import java.util.Optional;

import com.snack.dto.ControllerSuborderDTO;
import com.snack.dto.ControllerSuborderLineDTO;
import com.snack.dto.OrderDTO;
import com.snack.dto.RegisterTotalOrderDto;
import com.snack.entities.DeliveryType;
import com.snack.entities.Order;
import com.snack.entities.OrderStatus;
import com.snack.entities.PaymentType;
import com.snack.entities.SuborderLineStatus;
import com.snack.entities.SuborderStatus;
import com.snack.repositories.DeliveryTypeRepository;
import com.snack.repositories.OrderRepository;
import com.snack.repositories.OrderStatusRepository;
import com.snack.repositories.PaymentTypeRepository;
import com.snack.repositories.SuborderLineStatusRepository;
import com.snack.repositories.SuborderStatusRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class OrderService {
    
    @Autowired
    private OrderRepository orderRepository;
    @Autowired
    private OrderStatusRepository orderStatusRepository;

    @Autowired
    private PaymentTypeRepository paymentTypeRepository;

    @Autowired
    private DeliveryTypeRepository deliveryTypeRepository;

    @Autowired
    private SuborderStatusRepository suborderStatusRepository;

    @Autowired
    private SuborderLineStatusRepository suborderLineStatusRepository;

    public List<Order> getAllOrdersByIdCustomerIdOrderStatus(Long idCustomer, Integer idOrderStatus){
        return orderRepository.getAllOrdersByIdCustomerIdOrderStatus(idCustomer,idOrderStatus);
    }

    public Order saveOrder(Long idCustomer, OrderDTO orderdto){

            Order order = new Order();
            OrderStatus orderstatus = new OrderStatus();
            orderstatus.setIdOrderStatus(orderdto.getIdOrderStatus());
            order.setTotal(orderdto.getTotal());
            order.setIdCustomer(idCustomer);
            order.setOrderDate(orderdto.getOrderDate());
            order.setOrderHour(orderdto.getOrderHour());
            order.setOrderStatus(orderstatus);
            return orderRepository.save(order);

    }

    public Integer findForeignKeysOrder(RegisterTotalOrderDto registerTotalOrderDto){

        Optional <OrderStatus> optional = orderStatusRepository.findById(registerTotalOrderDto.getOrderidOrderStatus());
        if(optional.isPresent()){

            for(ControllerSuborderDTO controllerSuborderDTO : registerTotalOrderDto.getControllerSuborderDto()){

                Optional <PaymentType> optionalPaymentType= paymentTypeRepository.findById(controllerSuborderDTO.getSuborderidPaymentType());
                Optional <DeliveryType> optionalDeliveryType= deliveryTypeRepository.findById(controllerSuborderDTO.getSuborderidDeliveryType());
                Optional <SuborderStatus> optionalSuborderStatus= suborderStatusRepository.findById(controllerSuborderDTO.getSuborderidSuborderStatus());
                //Optional <Order> optionalOrder= orderRepository.findById(idOrder);

                if(optionalPaymentType.isPresent() & optionalDeliveryType.isPresent() & optionalSuborderStatus.isPresent() ){

                    for(ControllerSuborderLineDTO controllserSuborderLineDTO: controllerSuborderDTO.getControllerSuborderLineDto()){

                        //Optional <Suborder> optionalSuborder= suborderRepository.findById(idSuborder);
                        Optional <SuborderLineStatus>optionalSuborderLineStatus= suborderLineStatusRepository.findById(controllserSuborderLineDTO.getSuborderlineidSuborderLineStatus());
                        if(optionalSuborderLineStatus.isPresent() ){
                            
                        }
                        else{
                            return 0;
                        }
                    }
                }
                else{
                    return 0;
                }
            }
            return 1;
        }
        else{
            return 0;
        }
    }
}