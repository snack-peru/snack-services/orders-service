
package com.snack.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

//Daniel
@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "suborder")

public class Suborder {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="idSuborder")
    private Long idSuborder;
    

    @Column(name="displayId")
    private String displayId;

    @Column(name="subtotal")
    private Double subtotal;

    @Column(name="deliveryPrice")
    private Double deliveryPrice;

    @Column(name="total")
    private Double total;

    @Column(name="quantity")
    private Integer quantity;

    @Column(name="idStore")
    private Integer idStore;

    @Column(name="idCustomer")
    private Long idCustomer;

    @ManyToOne
    @JoinColumn(name="idPaymentType")
    private PaymentType paymentType;

    @ManyToOne
    @JoinColumn(name="idDeliveryType")
    private DeliveryType deliveryType;

    @ManyToOne
    @JoinColumn(name="idSuborderStatus")
    private SuborderStatus suborderStatus;

    @ManyToOne
    @JoinColumn(name="idOrder")
    private Order order;


}