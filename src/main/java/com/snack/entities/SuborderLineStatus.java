package com.snack.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

//Daniel
@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "suborderLineStatus")
public class SuborderLineStatus {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "idSuborderLineStatus")
    private Integer idSuborderLineStatus;

    @Column(name = "name")
    private String name;

    @Column(name = "description")
    private String description;
    
}