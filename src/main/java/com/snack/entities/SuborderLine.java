package com.snack.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

//Daniel
@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "suborderLine")
public class SuborderLine {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "idSuborderLine")
    private Long idSuborderLine;

    @Column(name = "quantityBought")
    private Integer quantityBought;

    @Column(name = "unitPrice")
    private Double unitPrice;

    @Column(name = "quantityAvailable")
    private Integer quantityAvailable;

    @Column(name="idStore")
    private Integer idStore;

    @Column(name="idProduct")
    private Long idProduct;

    @ManyToOne
    @JoinColumn(name = "idSuborder")
    private Suborder suborder;

    @ManyToOne
    @JoinColumn(name = "idSuborderLineStatus")
    private SuborderLineStatus suborderLineStatus;

}
