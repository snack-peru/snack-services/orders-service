package com.snack.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

//Daniel
@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name="paymentType")
public class PaymentType{
    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    @Column(name="idPaymentType")
    private Integer idPaymentType;

    @Column(name = "name")
    private String name;

    @Column(name = "description")
    private String description;

    @Column(name = "type")
    private String type;

}