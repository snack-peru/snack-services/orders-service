package com.snack.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

//Daniel
@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "deliveryType")
public class DeliveryType{
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column (name="idDeliveryType")
    private Integer idDeliveryType;

    @Column(name = "name")
    private String name;

    @Column(name = "description")
    private String description;

}