package com.snack.entities.mongo;

import java.util.List;

import lombok.Builder;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Builder
@Document(collection = "basket")
public class Basket {
    @Id
    private String basketId;
    private Long customerId;
    private List<Store> stores;
    private List<Product> productsAlone; // productos sin tienda asignada
}