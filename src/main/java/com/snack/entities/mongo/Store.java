package com.snack.entities.mongo;

import java.util.List;

import lombok.Builder;
import org.springframework.data.mongodb.core.mapping.Document;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Builder
@Document(collection = "store")
public class Store {
    private Integer storeId;
    private String name;
    private List<Product> products;
    private String deliverytype;
}