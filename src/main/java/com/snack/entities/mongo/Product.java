package com.snack.entities.mongo;

import lombok.Builder;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Builder
@Document(collection = "product")
public class Product {
    private Long productId;
    private String brand;
    private String name;
    //TODO: Validar si usar Integer o Double
    private Integer quantity;
    private String measurement;
}